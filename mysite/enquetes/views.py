from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.views import generic
from django.utils import timezone
from .models import Pergunta, Opcao

class IndexView(generic.View):
    def get(self, request, *args, **kwargs):
        lista_perguntas = Pergunta.objects.filter(
            data_publicacao__lte = timezone.now()
        ).order_by('-data_publicacao')[:6]
        contexto = {'lista_perguntas': lista_perguntas,}
        return render(request, 'enquetes/index.html', contexto)

class DetalhesView(generic.View):
    def get(self, request, *args, **kwargs):
        id = self.kwargs['id']
        pergunta = get_object_or_404(Pergunta, pk=id)
        contexto = {'pergunta': pergunta,}
        return render(request, 'enquetes/detalhes.html', contexto)

class ResultadoView(generic.View):
    def get(self, request, *args, **kwargs):
        id = self.kwargs['id']
        pergunta = get_object_or_404(Pergunta, pk=id)
        contexto = {'pergunta': pergunta,}
        return render(request, 'enquetes/resultado.html', contexto)

class VotacaoView(generic.View):
    def post(self, request, *args, **kwargs):
        id = self.kwargs['id']
        pergunta = get_object_or_404(Pergunta, pk=id)
        try:
            selecionada = pergunta.opcao_set.get(pk=request.POST['id_opcao'])
        except (KeyError, Opcao.DoesNotExiste):
            contexto = {
                'pergunta': pergunta,
                'error_message': "Selecione uma opção válida",
            }
            return render(request, 'enquetes:detalhes', contexto)
        else:
            selecionada.quant_votos += 1
            selecionada.save()
            return HttpResponseRedirect(
                reverse('enquetes:resultado', args=(pergunta.id,))
            )

"""
-------------------------------------------------------------
Formas alternativas de representação de elementos de view


### INDEXAlternativa 1: função de view INDEX
######################################

def index(request):
    lista_perguntas = Pergunta.objects.order_by('-data_publicacao')[:6]
    contexto = {'lista_perguntas': lista_perguntas,}
    return render(request, 'enquetes/index.html', contexto)



### INDEX Alternativa 2: extendendo uma classe de view genérica
########################################################
class IndexView(generic.ListView):
    template_name = 'enquetes/index.html'
    context_object_name = 'lista_perguntas'
    def get_queryset(self):
        return Pergunta.objects.order_by('-data_publicacao')[:6]

------------------------------------------------------------------------
### DETALHES Alternativa 1: função de view
########################################################
def detalhes(request, id_pergunta):
    pergunta = get_object_or_404(Pergunta, pk=id_pergunta)
    contexto = { 'pergunta': pergunta, }
    return render(request, 'enquetes/detalhes.html', contexto)

### DETALHES Alternativa 2: extendendo uma classe de view genérica
########################################################

class DetalhesView(generic.DetailView):
    model = Pergunta
    template_name = 'enquetes/detalhes.html'

------------------------------------------------------------------------
### RESULTADO Alternativa 1: função de view
########################################################
def resultado(request, id_pergunta):
    pergunta = get_object_or_404(Pergunta, pk=id_pergunta)
    contexto = {'pergunta': pergunta,}
    return render(request, 'enquetes/resultado.html', contexto)

### RESULTADO Alternativa 2: extendendo uma classe de view genérica
########################################################
class ResultadoView(generic.DetailView):
    model = Pergunta
    template_name = 'enquetes/resultado.html'

---------------------------------------------------------------------
### VOTACAO: na forma de uma função view
########################################################
def votacao(request, id_pergunta):
    pergunta = get_object_or_404(Pergunta, pk=id_pergunta)
    try:
        selecionada = pergunta.opcao_set.get(pk=request.POST['id_opcao'])
    except (KeyError, Opcao.DoesNotExiste):
        contexto = {
            'pergunta': pergunta, 'error_message': "Selecione uma opção válida"
        }
        return render(request, 'enquetes:detalhes', contexto)
    else:
        selecionada.quant_votos += 1
        selecionada.save()
        return HttpResponseRedirect(
            reverse('enquetes: resultado', args=(pergunta.id,))
        )
"""