from django.test import TestCase
from django.utils import timezone
from django.urls import reverse
import datetime
from .models import Pergunta

def criar_pergunta(texto, dias):
    """
    Cria um objeto pergunta informando um texto é uma quantidade de dias,
    que pode ser positiva(futuro) ou negativa(passado) a partir do dia atual.
    """
    data = timezone.now() + datetime.timedelta(days = dias)
    return Pergunta.objects.create(texto = texto, data_publicacao = data)

### Testes funcionais referentes a classe ViewIndex
###################################################
class IndexViewTest(TestCase):
    def test_sem_pergunta_cadastradas(self):
        """
        Se não existirem perguntas cadastradas é exibida uma mensagem específica.
        """
        resposta = self.client.get(reverse('enquetes:index'))
        self.assertEqual(resposta.status_code, 200)
        self.assertContains(resposta, "Nenhuma enquete cadastrada até o momento!")
        self.assertQuerysetEqual(resposta.context['lista_perguntas'], [])

    def test_com_pergunta_no_passado(self):
        """
        Pergunta com data de publicação no passado é exibida ao acessar a index.
        """
        criar_pergunta(texto='Pergunta no passado', dias=-30)
        resposta = self.client.get(reverse('enquetes:index'))
        self.assertEqual(resposta.status_code, 200)
        self.assertContains(resposta, "Pergunta no passado")
        self.assertQuerysetEqual(
            resposta.context['lista_perguntas'],
            ['<Pergunta: Pergunta no passado>']
        )

    def test_com_pergunta_no_futuro(self):
        """
        Pergunta com data de publicação no futuro NÃO é exibida na index.
        """
        criar_pergunta(texto='Pergunta no futuro', dias=30)
        resposta = self.client.get(reverse('enquetes:index'))
        self.assertEqual(resposta.status_code, 200)
        self.assertContains(resposta, "Nenhuma enquete cadastrada até o momento!")
        self.assertQuerysetEqual(resposta.context['lista_perguntas'], [])

    def test_pergunta_no_passado_e_outra_no_futuro(self):
        """
        A index exibe APENAS a pergunta com data de publicação no passado.
        """
        criar_pergunta(texto='Pergunta no passado', dias=-30)
        criar_pergunta(texto='Pergunta no futuro', dias=30)
        resposta = self.client.get(reverse('enquetes:index'))
        self.assertEqual(resposta.status_code, 200)
        self.assertContains(resposta, "Pergunta no passado")
        self.assertQuerysetEqual(
            resposta.context['lista_perguntas'],
            ['<Pergunta: Pergunta no passado>']
        )

    def test_duas_perguntas_no_passado(self):
        """
        São exibidas mais de uma pergunta com datade publicação no passado.
        """
        criar_pergunta(texto='Pergunta no passado 1', dias=-30)
        criar_pergunta(texto='Pergunta no passado 2', dias=-5)
        resposta = self.client.get(reverse('enquetes:index'))
        self.assertEqual(resposta.status_code, 200)
        self.assertQuerysetEqual(
            resposta.context['lista_perguntas'],
            ['<Pergunta: Pergunta no passado 2>',
            '<Pergunta: Pergunta no passado 1>']
        )