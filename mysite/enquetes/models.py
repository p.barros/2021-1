import datetime
from django.db import models
from django.utils import timezone


# Create your models here.
class Pergunta(models.Model):
    texto = models.CharField(max_length=200)
    # autor = models.CharField(max_length=80, null=True)
    data_publicacao = models.DateTimeField('Data de Publicação')
    # data_encerramento = models.DateField('Data de Encerramento', null=True)
    def __str__(self):
        return self.texto
    def publicada_recentemente(self):
        agora = timezone.now()
        passado_24hrs = agora - datetime.timedelta(days=1)
        return passado_24hrs <= self.data_publicacao <= agora

class Opcao(models.Model):
    texto = models.CharField(max_length=100)
    quant_votos = models.IntegerField(default=0)
    pergunta = models.ForeignKey(Pergunta, on_delete=models.CASCADE)
    def __str__(self):
        return self.texto